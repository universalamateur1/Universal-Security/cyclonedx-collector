# Group CycloneDX-Collector

Have all enabled Projects in this Group store their Sbom in this project.
One versin with the creation timestaamp and always the latest.

## Goals for this experiment

1. Have Sboms created by the depemndancy scanning job in Gitlab and collect them in a central place, so that those are seachable
1. Get the Sboms from Containerscanning if present
1. Combine the Sboms with JQ
1. Create a Pages deploly in the collector project, which displays the Sboms similar to Dependancy Track from Owasp

## Create Token

Create a Project Access Token for this collector Project

## Add Variables

Add group Variables:

1. `SBOM_Commit_Token:THE_VALUE_OF_THE_PAT`
1. `Path_to_Collector:The full without the https Path with .git at the end to this project`
  1. For example `Path_to_Collector:gitlab.com/universalamateur1/Universal-Security/cyclonedx-collector.git`

## Add script

Add this script snippet to the projects you want to enable and collect the SBOM from

```yaml
include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml

#Optional higher log level
variables:
  SECURE_LOG_LEVEL: debug

.cyclonedx-reports:
  before_script:
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  after_script:
    # Commit the file also to the collector repo
    # Group Level Token with Repo write permissions SBOM_Commit:$SBOM_Commit_Token
    # Cloning shallow into the central Group Repo
    - git clone https://SBOM_Commit:$SBOM_Commit_Token@$Path_to_Collector --depth 1
    # Creating folder for project and Sbom
    - mkdir -p cyclonedx-collector/$CI_PROJECT_NAME
    # Copy the Sbom over with date and as latest
    - cp ./gl-sbom-*.cdx.json ./cyclonedx-collector/$CI_PROJECT_NAME/gl-sbom-$CI_PROJECT_NAME-$CI_PIPELINE_CREATED_AT.cdx.json
    - cp ./gl-sbom-*.cdx.json ./cyclonedx-collector/$CI_PROJECT_NAME/gl-sbom-$CI_PROJECT_NAME-latest.cdx.json
    - cd cyclonedx-collector
    # Adding the Sboms to the git group repo
    - git add .
    - git status
    # Commiting and pushing to the Group repo
    - git commit -m "Project $CI_PROJECT_NAME updated Dependancy Scan at $CI_PIPELINE_CREATED_AT [ci skip] "
    - git push -o ci-skip https://SBOM_Commit:$SBOM_Commit_Token@$Path_to_Collector HEAD:main
```

## Variants
Possibility to use the Api for the commit of the file [√](https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions)
